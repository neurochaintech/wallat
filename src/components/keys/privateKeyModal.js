import React from "react"
import { Icon, Input, Modal } from "antd"

/**
 * @param {boolean} visibility switch the modal visibility
 * @param {function} onVisibilityChange callback called when modal is hidden
 */
class PrivateKeyModal extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      privateKey: "",
    }
  }

  hide(data) {
    if (this.props.onVisibilityChange) {
      this.props.onVisibilityChange(data)
    }
  }

  handleOk(event) {
    this.hide(this.state.privateKey)
  }

  handleCancel(event) {
    this.hide()
  }

  handlePrivateKey(event) {
    this.setState({ privateKey: event.target.value })
  }

  render() {
    return (
      <Modal
        title="Private key"
        visible={this.props.visibility}
        onOk={this.handleOk.bind(this)}
        onCancel={this.handleCancel.bind(this)}
      >
        <Input
          prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />}
          placeholder="Private Key"
          onChange={this.handlePrivateKey.bind(this)}
          value={this.state.privateKey}
        />
      </Modal>
    )
  }
}

export default PrivateKeyModal
