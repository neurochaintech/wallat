import React from "react"
import { Card, Icon, Input } from "antd"

const button = <Icon type="lock" />

class Address extends React.Component {
  handleAddress(value) {
    if (value !== "") {
      if (this.props.onAddress) {
        this.props.onAddress(value)
      }
    }
  }

  render() {
    return (
      <Card title="Public Key">
        <Input.Search
          placeholder="Public Key"
          onSearch={this.handleAddress.bind(this)}
          enterButton={button}
        />
      </Card>
    )
  }
}

export default Address
