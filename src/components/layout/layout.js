import React, { Component } from "react"
import "./layout.css"
import { Layout } from "antd"
import Logo from "./logo"
import WallatMenu from "./menu"
import Storage from "../../services/storage"

const { Header, Content } = Layout
const logoStyle = {
    width: "25%"
}
class WallatLayout extends Component {
  constructor(props) {
    super(props)
    this.state = {
      hasAddress: Storage.hasAddress,
    }

    Storage.onAddAddress = publicKey => this.setState({ hasAddress: true })
    Storage.onRemoveAddress = () => this.setState({ hasAddress: false })
  }
  render() {
    return (
      <div className="layout">
        <Layout className="container">
          <Header className="flex-header">
            <Logo style={logoStyle} />
            {this.state.hasAddress && <WallatMenu />}
          </Header>
          <Content> {this.props.children} </Content>
        </Layout>
      </div>
    )
  }
}

export default WallatLayout
