import React, { Component } from "react"
import { Icon, Menu } from "antd"
import { Link } from "gatsby"
import Storage from "../../services/storage"

class WallatMenu extends Component {
  render() {
    return (
      <Menu mode="horizontal">
        <Menu.SubMenu title={<Icon type="setting" className="large" />}>
          <Menu.Item>
            <Link to="/"> Home </Link>
          </Menu.Item>
          <Menu.Item>
            <Link to="/send"> Send coin </Link>
          </Menu.Item>
          <Menu.Divider />
          <Menu.Item>
            <Link to="/" onClick={Storage.removeAddress.bind(Storage)}>
              Change Address
            </Link>
          </Menu.Item>
        </Menu.SubMenu>
      </Menu>
    )
  }
}

export default WallatMenu
