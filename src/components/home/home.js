import React, { Component } from "react"
import { Card, Divider, Input, Spin, message } from "antd"
import QRCode from "qrcode.react"
import Storage from "../../services/storage"
import BotApi from "../../services/bot"
import "./home.css"

const Currency = (<span> NCC </span>)

class Home extends Component {
  constructor(props) {
    super(props)
    const address = Storage.getAddress()
    this.botApi = new BotApi(address)
    this.state = {
      address: address,
      nccAmount: props.balance,
    }
  }

  async changeAmount(promisedAmount) {
    this.setState({ nccAmount: false })
    this.setState({ nccAmount: await promisedAmount })
  }

  componentDidMount() {
    if (!this.state.nccAmount) {
      this.botApi
        .balance()
        .then(this.changeAmount.bind(this))
        .catch(e => {
          message.error(e.message)
          Storage.removeAddress(this.botApi.address)
        })
    }
  }

  render() {
    return (
      <Card className="home" title="Receive Payment">
        <QRCode value={this.state.address} level="M" size={128} />
        <div>
          {this.state.nccAmount ? (
            <Input
              addonBefore="balance "
              addonAfter={Currency}
              value={this.state.nccAmount.ncc}
              readOnly={true}
            />
          ) : (
            <Spin />
          )}
        </div>
        <Divider />
        {this.state.address}
      </Card>
    )
  }
}

export default Home
