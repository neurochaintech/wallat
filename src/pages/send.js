import React, { Component } from "react"
import { Button, Card, Form, Icon, Input, message } from "antd"
import Storage from "../services/storage"
import BotApi from "../services/bot"
import PrivateKeyModal from "../components/keys/privateKeyModal"
import * as Keys from "../services/keys"
import Transaction from "../services/transaction"
import { hex2str } from "../services/utils"
import { navigate } from "gatsby-link"
import NCC from "../services/ncc"

class Send extends Component {
  constructor(props) {
    super(props)
    const publicKey = Storage.getAddress()
    this.botApi = new BotApi(publicKey)
    this.state = {
      modalVisibility: false,
      preparedCall: null,
      loading: false,
      publishing: false
    }
  }

  handleSend = e => {
    e.preventDefault()
    const recipient = e.target.recipient.value
    let nccAmount = 0
    try {
      nccAmount = NCC.toPetit(e.target.nccamount.value)
    } catch (e) {
      message.error(e.message)
      return
    }
    const toSend = new Transaction(recipient, nccAmount.petit, "", 0)
    this.botApi
      .transaction(toSend)
      .then(payload => {
        const preparedCall = async key => {
          this.setState({ preparedCall: null })
          const str = hex2str(payload)
          const digest = Keys.hash(str)
          const signature = Keys.sign(digest, key)
          try {
            this.setState({ publishing: true })
            await this.botApi.sendTransaction(payload, signature)
            message.info(`sent ${nccAmount.ncc} ncc to ${recipient}`)
            this.props.form.resetFields()
            navigate("/")
          } catch (e) {
            message.error(`can't sent ${nccAmount.ncc} ncc to ${recipient}`)
          }
          this.setState({ publishing: false })
          this.setState({ modalVisibility: false })
        }
        this.setState({ preparedCall, modalVisibility: true, loading: false})
      })
      .catch(e => {
        this.setState({ publishing: false })
        this.setState({ loading: false })
        this.setState({ modalVisibility: false })
        message.error(e.message)
      })
    this.setState({loading: true})
  }

  handlePrivateKey(key) {
    this.setState({ modalVisibility: false })
    if (key && this.state.preparedCall) {
      this.state.preparedCall(key).catch(e => message.error(e))
    } else {
      console.error("missing payload ?")
    }
  }

  hasErrors() {
    const fieldsError = this.props.form.getFieldsError()
    const pristine = !this.props.form.isFieldsTouched()
    return Object.values(fieldsError).some(field => field) || pristine
  }

  render() {
    const { getFieldDecorator } = this.props.form

    const recipientInputRules = { required: true }
    const recipientInput = getFieldDecorator("recipient", {
      rules: [recipientInputRules],
    })(
      <Input prefix={<Icon type="qrcode" />} placeholder="Recipient Address" />
    )

    const nccInputRules = {
      required: true,
      pattern: /^\d{1,10}(.\d{1,9})?$/,  // type: "number" is broken ...
      message: "incorrect amount",
    }
    const nccInput = getFieldDecorator("nccamount", { rules: [nccInputRules] })(
      <Input
        prefix={<Icon type="pay-circle" rotate={180} />}
        placeholder="NCC Amount"
      />
    )

    return (
      <Card title="Send NCC" loading={this.state.loading}>
        <Form onSubmit={this.handleSend.bind(this)}>
          <Form.Item> {recipientInput} </Form.Item>
          <Form.Item> {nccInput} </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              disabled={this.hasErrors.bind(this)()}
            >
              Send
            </Button>
          </Form.Item>
          <PrivateKeyModal
            visibility={this.state.modalVisibility}
            onVisibilityChange={this.handlePrivateKey.bind(this)}
          />
        </Form>
      </Card>
    )
  }
}

export default Form.create()(Send)
