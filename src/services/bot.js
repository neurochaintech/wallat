import NCC from "./ncc"

class BotApi {
  constructor(pubkey) {
    if (typeof window !== `undefined`) {
      // this.url = `http://${window.location.hostname}:8080`
      this.url = `https://api.testnet.neurochaintech.io`
    }
    this.pubkey = pubkey
  }

  async validate() {
    const url = this.url
    const pubkey = this.pubkey
    try {
      const resp = fetch(`${url}/validate`, {
        method: "POST",
        body: JSON.stringify({rawData: pubkey})
      })
      const data = await resp
      return data.status
    } catch (e) {
      return Promise.reject({message: `can't connect to bot ${url}`})
    }
  }

  async balance() {
    const url = this.url
    const pubkey = this.pubkey
    try {
      const resp = fetch(`${url}/balance`, {
        method: "POST",
        body: JSON.stringify({rawData: pubkey})
      })
      const data = (await resp).json()
      const rawNcc = (await data).value
      return new NCC(rawNcc)
    } catch (e) {
      return Promise.reject({message: `Can't fetch balance for address ${pubkey}`})
    }
  }

  async transaction(...transaction) {
    const url = this.url
    const pubkey = this.pubkey
    const fee = 0
    const resp = fetch(`${url}/create_transaction`, {
      method: "POST",
      body: JSON.stringify({
        key_pub: {rawData: pubkey},
        outputs: transaction,
        fee
      })
    })
    return (await resp).text()
  }

  async sendTransaction(payload, signature) {
    const url = this.url
    const pubkey = this.pubkey
    try {
      const resp = await fetch(`${url}/publish`, {
        method: "POST",
        body: JSON.stringify({
            transaction: payload,
            signature: signature,
            keyPub: {rawData: pubkey}
        }),
      })
      if (!resp.ok) {
        return Promise.reject(await resp.text())
      }
    } catch (e) {
      return Promise.reject(e)
    }
  }

  async status() {
    const url = this.url
    try {
      const resp = fetch(`${url}/status`)
      const data = (await resp).json()
      return data
    } catch (e) {
      return Promise.reject(e)
    }
  }
}

export default BotApi
