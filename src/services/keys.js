import elliptic from "elliptic"
import { hex2b64 } from "./utils"

const ecdsa = new elliptic.ec("secp256k1")

export const sign = (message, key) => {
  const ecdsaKey = ecdsa.keyFromPrivate(key, "hex")
  const signature = ecdsaKey.sign(message)
  return signature.r.toString("hex") + signature.s.toString("hex")
}

export const derivePublicKey = key => {
  const ecdsaKey = ecdsa.keyFromPrivate(key, "hex")
  return ecdsaKey.getPublic().encode("hex")
}

export const compressedPublicKey = publicKey => {
  const x = publicKey.x.toString("hex")
  if (publicKey.y.isEven()) {
    return "02" + x
  } else {
    return "03" + x
  }
}

export const createKey = () => {
  const key = ecdsa.genKeyPair()
  const exponent = key.getPrivate("hex")
  const publicKey = compressedPublicKey(key.getPublic())
  return {
    private: exponent,
    public: hex2b64(publicKey),
  }
}

export const loadKey = keyData => {
  const privateKey = ecdsa.keyFromPrivate(keyData.private, "hex")
  const publicKey = ecdsa.keyFromPublic(keyData.public, "hex")
  privateKey.pub = publicKey.pub
  return privateKey
}

export const hash = message => ecdsa.hash().update(message).digest("hex")
