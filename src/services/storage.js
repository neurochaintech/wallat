class Storage {
  constructor() {
    if (typeof window !== `undefined`) {
      this.hasAddress = sessionStorage.getItem("address") !== null
    }
    this._onRemoveAddress = []
    this._onAddAddress = []
  }

  getAddress() {
    if (typeof window !== `undefined`) {
      const address = sessionStorage.getItem("address")
      if (address) {
        return address
      } else {
        // throw ?
      }
    }
  }

  addAddress(address) {
    if (typeof window !== `undefined`) {
      sessionStorage.setItem("address", address)
    }
    this.hasAddress = true
    for (const cb of this._onAddAddress) {
      cb(address)
    }
  }

  removeAddress() {
    if (typeof window !== `undefined`) {
      sessionStorage.removeItem("address")
    }
    this.hasAddress = false
    for (const cb of this._onRemoveAddress) {
      cb()
    }
  }

  set onRemoveAddress(cb) {
    this._onRemoveAddress.push(cb)
  }

  set onAddAddress(cb) {
    this._onAddAddress.push(cb)
  }
}

export default new Storage()
