export const hex2str = hex => {
  let str = []
  for (let i = 0; i < hex.length; i += 2) {
    str.push(parseInt(hex.substr(i, 2), 16));
  }
  return str
}

export const hex2b64 = hex => {
  let str = []
  for (let i = 0; i < hex.length; i += 2) {
    str.push(String.fromCharCode(parseInt(hex.substr(i, 2), 16)))
  }
  return btoa(str.join(""))
}
