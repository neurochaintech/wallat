class Transaction {
  constructor(keypub, value, data, fee) {
    this.key_pub = {rawData: keypub}
    this.value = {value: value.toString()}
    this.data = data
  }
}

export default Transaction
