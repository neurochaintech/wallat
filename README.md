## 🚀 Quick start
1.  **install dependency**
    ```sh
    $ yarn
    ```
    
2.  **Run in develop mode**

    ```sh
    $ yarn run develop
    ```
    the server should be running at `http://localhost:8000`

2. **Run in prod mode**
    ```sh
   $ yarn run build
   $ yarn run serve
   ```
   the server should be running at `http://localhost:9000`
   
## 👛 How to use
* ensure that a bot is launched on the same machine that the wallat
* open the wallat (you should be offered to insert your public key)
* insert your public key (it should be a base64 string)
and wait for the bot to get your balance
* if you want to send a few coin to someone go in the top right menu (⛭)
and select `send NCC`
* here you can insert the recipient address (it's public key as base64 string)
and the amount you want to send
* when you are sure about the ncc you want to send click on send button
* you are asked to type your private key, it is the `private exponent` you should have
type it and click send again to send the transaction

## TODO
* add a way to insert data in transaction
* better error message
* is the qr code usefull ?
* css rework
