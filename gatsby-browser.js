import "./src/styles/reset.css"
import "antd/dist/antd.css"
import React from "react"
import WallatLayout from "./src/components/layout/layout"

export const wrapPageElement = ({ element, props }) => {
  return <WallatLayout {...props}> {element} </WallatLayout>
}
